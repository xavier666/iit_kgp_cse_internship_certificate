# Certificate template issued to interns on completion

## How to generate a certificate?

1. Check the **Usage** section and make the necessary changes
2. Save the `tex` file
3. Run `make`

Certificate should be generated as a `pdf` file

## Usage

Just change the `\newcommand` contents in `intern_cert.tex`

The parameters which can be changed are

1. `internName`
2. `internGender` - (His/Her)
3. `internCollege`
4. `internStartDate`
5. `internEndDate`
6. `projectTitle`
7. `internRating` - (Satisfactory/Good/Excellent)
8. `supervisorName`
9. `supervisorDesign` - (Assistant Professor/Associate Professor/Professor)
10. `signatureDate`

## To do

1. Add support for batch certificate creation
2. Support for `\vbox` for supervisor signature
